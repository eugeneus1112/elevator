<?php 

class Elevator {
	private $_callsQueue = array(); // queue of floors where the elevator was called from
	private $_destinationsQueue = array(); // queue of passengers destinations
	private $_tasks = array();
	
	function run($currentFloorNumber = 1) {	
		$destinationFloorNumber = $this->getDestinationFloorNumber($currentFloorNumber); // find the next destination
		if ($destinationFloorNumber === false) {
			throw new Exception("Destination not found for floor $currentFloorNumber");
		} 
		echo "Starting from floor $currentFloorNumber". DELIM;	
		$this->removeCall($currentFloorNumber); // removing start floor from the calls stack
		// Add destination to destinations stack			
		$this->addDestination($destinationFloorNumber);
		$startFloorNumber = $currentFloorNumber;
		// Moving until the calls stack and destinations stack is empty
		do {
			// Get next stop floor 
		    $nextFloorNumber = $this->getNextStopFloor($startFloorNumber);		 
		    echo "Next stop: floor $nextFloorNumber". DELIM;
		    echo "Moving...". DELIM;
		    $this->moveToNextStop($nextFloorNumber);
		    $startFloorNumber = $nextFloorNumber;		    
		}while (count($this->_callsQueue) > 0 || count($this->_destinationsQueue) > 0);   
	}
	
	function moveToNextStop($floorNumber) {
		echo "Arrived to floor $floorNumber". DELIM;
		// exclude the floor number out of calls queue
		$this->removeCall($floorNumber);
		// arrived to the destination, exclude the floor number out of destionations queue
		if ($this->isDestination($floorNumber)) {
			 echo "Passenger come out". DELIM;
		     $this->removeDestination($floorNumber);
		}
		// Find the destination of newcomer from task list
		$destinationFloorNumber = $this->getDestinationFloorNumber($floorNumber);
		if ($destinationFloorNumber) {
			echo "New passenger come in. Pressed floor $destinationFloorNumber". DELIM;
			// Adding the new destination of newcomer
			$this->addDestination($destinationFloorNumber);
		}
	}
	
	function getNextStopFloor($startFloorNumber) {
		if (count($this->_destinationsQueue)) {
			$destinationFloorNumber = reset($this->_destinationsQueue);			
		}else {
			$destinationFloorNumber = reset($this->_callsQueue);	
		}		
		$stops = array();	
		// Get all possible stops on the current route
		$possibleStops = $this->getAllStops($startFloorNumber, $destinationFloorNumber);
		// Defining all floors on the route where the elevator must stop
		$stops = array_intersect(array_merge($this->_destinationsQueue, $this->_callsQueue), $possibleStops);
		if (count($stops)) {
			// Find and return the closest stop on the route (next stop)
		    return $startFloorNumber < $destinationFloorNumber?min($stops):max($stops);
		}else {
			throw new Exception("Can't find the next stop");
		}
	}
	
	private function getAllStops($startFloor, $endFloor) {
        $return = array();
        if ($startFloor < $endFloor) {
            for ($i = $startFloor; $i <= $endFloor; $i ++) {
        	    if ($i != $startFloor) $return[] = $i;
            }
        }else {
            for ($i = $startFloor; $i >= $endFloor; $i --) {
        	    if ($i != $startFloor) $return[] = $i;
            }
        }
        return $return;	
	}
	
	// Getting the current passenger destination from task list
	function getDestinationFloorNumber($floorNumber) {
		if (isset($this->_tasks[$floorNumber])) {
		    $destinationFloorNumber = $this->_tasks[$floorNumber];
		    unset($this->_tasks[$floorNumber]);
		    return $destinationFloorNumber;
		}else {
			return false;
		}		
	}
	
	public function addCall($floorNumber) {
		$this->_callsQueue[] = $floorNumber;
	}
	
	private function addDestination($floorNumber) {
		$this->_destinationsQueue[] = $floorNumber;
	}
	
	private function removeCall($floorNumber) {
		$floorKey = array_search($floorNumber, $this->_callsQueue);
		if ($floorKey !== false) unset($this->_callsQueue[$floorKey]);
	}
	
	private function isDestination($floorNumber) {
		return array_search($floorNumber, $this->_destinationsQueue) !== false;
	}
	
	private function removeDestination($floorNumber) {
		$floorKey = array_search($floorNumber, $this->_destinationsQueue);
		if ($floorKey !== false) unset($this->_destinationsQueue[$floorKey]);
	}
	
	public function addTask($startFloorNumber, $destinationFloorNumber) {
		$this->_tasks[$startFloorNumber] = $destinationFloorNumber;
	}
}

?>