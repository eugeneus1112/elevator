<?php 

set_time_limit(0);
ob_implicit_flush();
include __DIR__. '/config.php';

$tasks = array();
for ($i = 1; $i <= 3; $i ++) {
	$success = false;
	do {
	   $floor = strtolower(readline("Input user $i floor [". $defaultSettings[$i - 1][0]. "] : "));
	   if (!is_numeric($floor)) {
	       echo "User floor can be only integer value". DELIM;	
	   }elseif (isset($tasks[$floor])) {
	   	   echo "Floor already existing is tasks". DELIM;
	   }else {
	       $success = true;
	   }
	}while (!$success); 
	
	$success = false;
	do {
	    $destinationFloor = strtolower(readline("Input user $i destination floor [". $defaultSettings[$i - 1][1]. "] : "));
	    if (!is_numeric($destinationFloor)) {
	        echo "User destination floor can be only integer value". DELIM;	
	    }elseif (in_array($destinationFloor, $tasks)) {
	    	echo "Destination was already set". DELIM;
	    }else {
	    	$success = true;
	    }
	}while (!$success);
	
	$tasks[$floor] = $destinationFloor;
}

if (!count($tasks)) {
	exit("Task list is empty");
}

require __DIR__. '/classes/elevator.class.php';

$elevator = new Elevator();
foreach ($tasks as $startFloor => $endFloor) {
	$elevator->addCall($startFloor); // adding to call floors stack
	$elevator->addTask($startFloor, $endFloor); // adding task
}
try {
  // Start to move
  $elevator->run();
}catch(Exception $e) {
	echo "Error: ". $e->getMessage(). DELIM;
	exit();
} 
echo "Finish!";

?>